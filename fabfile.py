from fabric.api import local, env

def production():
    env['remote'] = 'production'

# production is default target for now
production()

def deploy():
    """
    Deploy to remote

    """
    local(('ansible-playbook deployment/playbooks/deploy.yml '
           '-i deployment/playbooks/{0}_hosts').format(env['remote']))