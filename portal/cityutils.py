#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests, re

from django.utils.text import slugify

# gets the city from the location as opposed to the string set by
# the user, rest is the object returned by the resmio API
def restaurant_to_city_info(rest):
    point = rest['location']
    if not point:
        raise ValueError('restaurant does not have a location')
    lng, lat = point

    # google geocoding API request
    url = ('http://maps.googleapis.com/maps/api/geocode/json?'
           'latlng=%s,%s&sensor=false&language=de' % (lat, lng) )
    response = requests.get(url).json()
    if response['status'] != 'OK':
        raise Exception('google api returned: ' + response['status'])


    components = response['results'][0]['address_components']

    def is_locality(comp):
        return 'locality' in comp['types']

    locality = filter(is_locality, components)
    if len(locality) < 1:
        print (point, lng, lat)
        print (rest['name'], rest['city'])
        raise Exception('no city found')
    locality = locality[0]
    return {
        'name': locality['long_name'],
        'id': slugify(locality['long_name']),
        'location': [float(lng), float(lat)]
    }

def restaurant_to_city_id(rest):
    return restaurant_to_city_info(rest)['id']

