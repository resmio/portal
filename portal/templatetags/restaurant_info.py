from django import template

from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.dates import WEEKDAYS_ABBR
from django.template.defaultfilters import truncatechars
from django.conf import settings

from furl import furl
import re

from portal.utils import geo_distance

register = template.Library()


@register.filter
def group_by(lst, n):
    res = zip(*[lst[i::n] for i in range(n)])
    l = len(lst)
    if l % n != 0:
        res.append(tuple(lst[-(l % n):]))
    return res


@register.filter
def get_opening_hours(restaurant):
    strings = [opening_hour_to_str(x) for x in restaurant['opening_hours']]
    return mark_safe('<br/>\n'.join(strings))


# helpers
def opening_hour_to_str(opening_hour):
    weekdays = opening_hour['weekdays']
    begins = opening_hour['begins']
    ends = opening_hour['ends']

    consecutive_week_days = len(weekdays) == weekdays[-1] - weekdays[0] + 1

    if len(weekdays) == 1:
        wd_str = weekday_to_str(weekdays[0])
    elif consecutive_week_days:
        wd_str = '%s - %s' % ( weekday_to_str(weekdays[0]), 
                               weekday_to_str(weekdays[-1]) )
    else:
        wd_str = ', '.join( map(weekday_to_str, weekdays) )

    return '%s: <span class="opening-hours-time">%s - %s</span>' % (
        wd_str, 
        escape(begins),
        escape(ends)
    )


def weekday_to_str(wd):
    return unicode(WEEKDAYS_ABBR[wd])


@register.filter
def get_image_path(restaurant):
    if restaurant['images']:
        return restaurant['images'][0]['image']
    else:
        return settings.DEFAULT_RESTAURANT_IMAGE


@register.filter
def distance_from(restaurant, point):
    destination = restaurant['location']
    destination = map(lambda s: float(s), destination)
    dest = geo_distance(point[1], point[0], destination[1], destination[0])
    if dest > 0.8:
        return '%s km' % round(dest, 1)
    else:
        return '%s m' % int(round(dest, 2) * 1000)

@register.simple_tag()
def distance_between(restaurant1, restaurant2):
    return distance_from(restaurant1, restaurant2['location'])


@register.simple_tag(takes_context=True)
def description(context, restaurant, char_limit=None):
    lang = context['request'].LANGUAGE_CODE
    descr_default = restaurant.get('description', '')
    descr_multi = restaurant.get('description_multilingual', {})
    descr_lang = descr_multi.get(lang, None)
    if descr_lang:
        descr = descr_lang
    else:
        descr = descr_default

    if char_limit:
        return truncatechars(descr, char_limit)
    else:
        return descr


@register.simple_tag(takes_context=True)
def button_code(context, restaurant):
    lang = context['request'].LANGUAGE_CODE
    code = restaurant['buttonCode']
    old_url = r'static\.resmio\.com/static/\w\w/button\.js'
    new_url = 'static.resmio.com/static/%s/button.js' % lang
    return mark_safe(re.sub(old_url, new_url, code))


@register.filter
def latitude(restaurant):
    # str is needed to avoid localization of number which uses ',' in german
    return str(float(restaurant['location'][1]))


@register.filter
def longitude(restaurant):
    return str(float(restaurant['location'][0]))


@register.simple_tag(takes_context=True)
def travel_url(context, facility):
    lang = context['request'].LANGUAGE_CODE
    path = 'dn' if lang == 'de' else lang
    base_bahn_url = "http://reiseauskunft.bahn.de/bin/query.exe/"
    return (
        furl(base_bahn_url)
        .add(path=path)
        .add(args={
            'country': 'DEU',
            'rt': 1,
            'use_realtime_filter': 1,
            'searchMode': 'NORMAL',
            'start': 1,
            'Z': ', '.join((facility['zip_code'],
                            facility['street'])).encode('utf-8'),
            'ZADR': 1
        })
    ).url
