from django import template
from django.conf import settings
from django.utils.http import urlquote

from portal.templatetags.restaurant_info import get_image_path

register = template.Library()

@register.simple_tag
def resized_image_of(obj, width, height=0):
    """
    get the image of either a city or a restaurant
    example: resized_image_of 'berlin' 300 100
    example2: resized_image_of some_restaurant 200

    """
    url = image_url(obj)
    imgresize_host = getattr(settings, 'IMGRESIZE_HOST')
    return "http://%s/%sx%s/%s" % (imgresize_host, width, height, url)

# helper
def image_url(obj):
    if isinstance(obj, basestring):
        # assume obj is a city key
        city = obj
        domain = getattr(settings, 'SITE_DOMAIN')
        url = 'http://%s/static/img/cities/%s_16_9_0.jpg' % (domain, city)
    else:
        # assume obj is a restaurant
        restaurant = obj
        url = get_image_path(restaurant)
    return urlquote(url, '')


