from django import template
from django.conf import settings
from django.core.urlresolvers import reverse

import re
from urllib import urlencode
from urlparse import urlparse, urlunparse, parse_qsl

from localeurl.templatetags.localeurl_tags import chlocale

from portal.constants import URL_PARAMETERS_KEPT
from portal.templatetags.restaurant_info import get_image_path

register = template.Library()

@register.simple_tag(takes_context=True)
def add_parameters(context, url):
    """
    add get parameters from current url to internal links
    for example if the page is loaded with embedded=true
    all internal links should contain embedded=true

    the parameters that should be taken from the current url
    are kept in constants.URL_PARAMETERS_KEPT

    """
    get_params = context['request'].GET
    # params of the request that have to be passed over to url
    params = {}
    for p in get_params:
        if p in URL_PARAMETERS_KEPT:
            params[p] = get_params[p]

    # add the params form the request to url
    url_parts = list(urlparse(url))
    query = dict(parse_qsl(url_parts[4]))
    query.update(params)

    url_parts[4] = urlencode(query)

    return urlunparse(url_parts)

@register.simple_tag(takes_context=True)
def link_to_restaurant(context, restaurant):
    base_url = reverse('restaurant', args=[restaurant['slug']])
    return add_parameters(context, base_url)

@register.filter
def external_link(url):
    """
    add http:// if not already present

    """
    if re.match('\s*^https?://', url):
        return url
    else:
        return 'http://' + url

@register.simple_tag(takes_context = True)
def set_language_link(context, language):
    path = context['request'].path
    return add_parameters( context, chlocale(path, language) )
