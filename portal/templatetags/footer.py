from django.template.context import Context
from django import template
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.template.loader import select_template
from portal.cities import SORTED_CITIES, CITIES
from portal.constants import FOOTER_CITIES_NUM
from portal.categories import translate_category
from portal.templatetags.links import add_parameters

register = template.Library()

@register.filter
def footer_city_list(_unused):
    # get the first FOOTER_CITIES_NUM cities ordered by number of restaurants
    cities = [CITIES[id] for id in SORTED_CITIES[0:FOOTER_CITIES_NUM]]

    # generate html
    res = '<div class="row">\n'
    res += '<h3>Restaurants in <strong>%s</strong></h3>\n' % _('Germany')
    for city in cities: 
        url = reverse('city_list', args=[city['id']]) 
        res += '<div class="col-lg-3 col-md-4 col-sm-6">\n'
        res += ('  <a href="%s">Restaurants in <strong>%s</strong></a>\n'
                % (url, city['name']) )
        res += '</div>\n'
    res += '</div>\n'
    return mark_safe(res)


@register.simple_tag(takes_context=True)
def footer_category_list(context, city):
    # works both if the city info or the city id get passed
    if type(city) in [str, unicode]:
        city = CITIES[city]

    if not city['categories']:
        return ''
    res = '<div class="row">\n'
    res += '<h3> Restaurants in <strong>%s</strong></h3>\n' % city['name']
    for category in city['categories']:
        url = reverse('category_list', args=[city['id'], category])  
        res += '<div class="col-md-4 col-sm-6">\n'
        res += ('  <a href="%s">Restaurants in %s: <strong>%s</strong></a>\n'
                % (add_parameters(context, url),
                   city['name'],
                   translate_category(category) ))
        res += '</div>\n'
    res += '</div>\n'
    return mark_safe(res)


@register.simple_tag()
def city_description(city):
    t = select_template(['cities/%s.html' % city, 'cities/default.html'])
    return t.render(Context())
