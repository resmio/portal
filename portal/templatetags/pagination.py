from django import template
from django.utils.safestring import mark_safe
from django.utils.html import escape

from portal.templatetags.links import add_parameters

register = template.Library()

@register.simple_tag(takes_context=True)
def pagination(context, meta, base_url):
    # values in meta and base_url should be escaped!
    base_url = escape(base_url)

    is_first_page = not meta['previous']
    is_last_page = not meta['next']
    
    # don't add pagination if there is only one page
    if is_first_page and is_last_page:
        return ''

    # should already be ints, but make sure for protection against XSS
    offset = int(meta['offset'])
    limit = int(meta['limit'])
    total_count = int(meta['total_count'])

    page_count = (total_count - 1) / limit + 1
    current_page = offset / limit + 1


    # abstract representation of the <li> items of the pager
    # includes all pages and the previous and next buttons
    # each item has
    # - 'content' the content
    # - 'link_to' the page number (optional)
    # - 'class' (optional)
    # will expand to 
    # <li class="{{class}}"><a href="{{link}}">{{content}}</a></li>
    items = []

    # previous
    prev_item = {'content': '&laquo;'}
    if is_first_page:
        prev_item['class'] = 'disabled'
    else:
        prev_item['link_to'] = current_page - 1
    items.append(prev_item)
    
    # pages
    for page in range(1, page_count+1):
        item = {'content': str(page), 'link_to': page}
        if page == current_page:
            item['class'] = 'active'
        items.append(item)

    # next
    next_item = {'content': '&raquo;'}
    if is_last_page:
        next_item['class'] = 'disabled'
    else:
        next_item['link_to'] = current_page + 1
    items.append(next_item)

    # generate html
    res = '<ul class="pagination card-like">\n'
    for item in items:
        content = item['content']
        if item.has_key('link_to'):
            page = item['link_to']
            offset = (page-1) * limit
            url = '%soffset=%s&limit=%s' % (base_url, offset, limit)
        else:
            url = '#'
        # space before class required!
        if item.has_key('class'):
            # space required!
            class_info = ' class="%s"' % item['class']
        else:
            class_info = ''
        url = escape(add_parameters(context, url))
        res += '<li%s><a href="%s">%s</a></li>\n' % (class_info, url, content)
    res += '</ul>\n'

    return mark_safe(res)


@register.simple_tag
def prev_next_links(meta, base_url):
    """
    Add <link rel="prev/next" ...> links, to be added in the <header>

    """
    base_url = escape(base_url)

    is_first_page = not meta['previous']
    is_last_page = not meta['next']

    # should already ints, but make sure for protection against XSS
    offset = int(meta['offset'])
    limit = int(meta['limit'])

    res = ''
    if not is_first_page:
        url = '%soffset=%s&limit=%s' % (base_url, offset-limit, limit)
        res += '<link rel="prev" href="%s" />\n' % escape(url)
    if not is_last_page:
        url = '%soffset=%s&limit=%s' % (base_url, offset+limit, limit)
        res += '<link rel="next" href="%s" />\n' % escape(url)
    return mark_safe(res)
