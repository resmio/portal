# number of cities in the footer
FOOTER_CITIES_NUM = 28

# url parameter added to all internal links
# for example if the page is loaded with embedded=true
# all internal links should contain embedded=true
URL_PARAMETERS_KEPT = {'embedded', 'cebit', 'ligna', 'hannovermesse'}