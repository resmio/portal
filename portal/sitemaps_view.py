# vastly simplified code from
# django.contrib.sitemaps.views
# rewritten to not require the sites framework
# much code we don't need has been removed

from functools import wraps

from django.template.response import TemplateResponse
from django.http import HttpRequest

def x_robots_tag(func):
    @wraps(func)
    def inner(request, *args, **kwargs):
        response = func(request, *args, **kwargs)
        response['X-Robots-Tag'] = 'noindex, noodp, noarchive'
        return response
    return inner

@x_robots_tag
def sitemap_view(request, sitemaps, section=None,
            template_name='sitemap.xml', content_type='application/xml'):

    sitemap_objects = sitemaps.values()
    # urls and their info
    # [{'location': '/home', 'priority': '1.0'}, {...}, ...]
    urls = []
    for sitemap in sitemap_objects:
        def get_url(page):
            url = HttpRequest.build_absolute_uri(request, 
                                                 sitemap.location(page))
            return {
                'location': url,
                'priority': sitemap.priority(page),
                'changefreq': sitemap.changefreq(page)
            }
        urls.extend( map(get_url, sitemap.items()) ) 
    response = TemplateResponse(request, template_name, {'urlset': urls},
                                content_type=content_type)
    return response