from django.core.urlresolvers import reverse
from cities import CITIES, SORTED_CITIES


class BaseSitemap():
    def changefreq(self, page):
        return "monthly"


class SinglePagesSitemap(BaseSitemap):
    def items(self):
        # return the view names
        return ['home']

    def location(self, page):
        return reverse(page)

    def priority(self, page):
        if page == 'home':
            return '1.0'
        else:
            return '0.5'

    def changefreq(self, page):
        "weekly"


class CitiesSitemap(BaseSitemap):
    def items(self):
        return SORTED_CITIES

    def location(self, page):
        return reverse('city_list', kwargs={'city': page})

    def priority(self, page):
        return '0.6'


class CategoriesSitemap(BaseSitemap):
    def items(self):
        res = []
        for city in CITIES.values():
            for category in city['categories']:
                res.append([city['id'], category])
        return res

    def location(self, page):
        city, category = page
        return reverse('category_list', 
                       kwargs={'city': city, 'category': category})

    def priority(self, page):
        return '0.4'


sitemaps = {
    'single_pages': SinglePagesSitemap(),
    'cities': CitiesSitemap(),
    'categories': CategoriesSitemap(),
}
