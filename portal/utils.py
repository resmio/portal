from math import radians, sqrt, sin, cos, atan2

from portal.cities import CITIES

def geo_distance(lat1, lon1, lat2, lon2):
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon1 - lon2

    EARTH_R = 6372.8

    y = sqrt(
        (cos(lat2) * sin(dlon)) ** 2
        + (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon)) ** 2
        )
    x = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)
    c = atan2(y, x)
    return EARTH_R * c

def nearest_city(restaurant):
    """
    Among all cities that we know about, return the most nearby one

    """
    lon, lat = restaurant['location']
    def distance(city):
        try:
            lon2, lat2 = city['location']
        except Exception as e:
            import pdb; pdb.set_trace()
        dist_from_center = geo_distance(lon, lat, lon2, lat2)
        dist_from_border = dist_from_center - city['radius']
        return dist_from_border

    city = min(CITIES.values(), key=distance)
    return city

