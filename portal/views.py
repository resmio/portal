from django.http import Http404
from django.shortcuts import render

from portal.utils import nearest_city
from portal.categories import translate_category

import requests

from cities import CITIES, SORTED_CITIES
from features_restaurants import featured_restaurants

PAGINATION_LIMIT = 21


def landing_page(request):
    cities = []
    for city_key in SORTED_CITIES[0:20]:
        cities.append({'city_key': city_key, 'name': CITIES[city_key]['name']})
    return render(request, 'landing_page.html', {
        'cities_top': cities[0:6],
        'cities20': cities,
        'redirect_to_nearby': False,
        'featured_restaurants': featured_restaurants})


def home(request):
    return landing_page(request)


def impressum(request):
    return render(request, 'impressum.html')


def city_list(request, city):
    try:
        city_info = CITIES[city]
        lon, lat = city_info['location']
    except KeyError:
        raise Http404('The city "%s" does not exist in our database' % city)

    offset = request.GET.get('offset', default=0)
    limit = request.GET.get('limit', default=PAGINATION_LIMIT)
    dist = int(city_info.get('radius', 5) * 1000)
    url = 'https://api.resmio.com/v1/facility?near=%s,%s&verified=true' \
          '&offset=%s&limit=%s&distance=%s' % (lon, lat, offset, limit, dist)
    response = requests.get(url).json()
    restaurants = response['objects']
    meta = response['meta']

    base_url = '%s?' % request.path
    return render(request, 'city_list.html', {
        'city': city_info,
        'city_key': city,
        'pagination_meta': meta,
        'pagination_base_url': base_url,
        'restaurants': restaurants})

def category_list(request, city, category):
    try:
        city_info = CITIES[city]
        lon, lat = city_info['location']
    except KeyError:
        raise Http404('The city "%s" does not exist in our database' % city)

    translated_category = translate_category(category)
    offset = request.GET.get('offset', default=0)
    limit = request.GET.get('limit', default=PAGINATION_LIMIT)
    dist = int(city_info.get('radius', 5) * 1000)
    url = ('https://api.resmio.com/v1/facility?near=%s,%s&category=%s' 
           '&verified=true&offset=%s&limit=%s&distance=%s' % 
           (lon, lat, category, offset, limit, dist))	

    response = requests.get(url).json()
    restaurants = response['objects']
    meta = response['meta']

    base_url = '/%s/%s?' % (city, category)
    return render(request, 'category_list.html', {
        'city': city_info,
        'city_key': city,
        'category': translated_category,
        'pagination_meta': meta,
        'pagination_base_url': base_url,
        'restaurants': restaurants})


def restaurant(request, restaurant_id): 
    url = 'https://api.resmio.com/v1/facility/%s' % restaurant_id
    try:
        rest = requests.get(url).json()
    except ValueError:
        raise Http404('The restaurant "%s" does not exist in our database' 
                      % restaurant_id)

    longitude, latitude = rest['location']

    url = ('https://api.resmio.com/v1/facility?near=%s,%s'
           '&verified=true&offset=0&limit=4&' %
           (longitude, latitude))

    # get the first three restaurants excluding the first
    # (which will be the restaurant itself)
    nearby_restaurants = requests.get(url).json()['objects'][1:4]
    city_info = nearest_city(rest)
    print repr(city_info)
    return render(request, 'restaurant.html', {
        'restaurant': rest,
        'latitude': str(rest['location'][1]),
        'longitude': str(rest['location'][0]),
        'city_key': city_info['id'],
        'city_name': city_info['name'],
        'nearby_restaurants': nearby_restaurants})


def nearby(request):
    longitude = request.GET.get('longitude')
    latitude = request.GET.get('latitude')
    has_coordinates = longitude and latitude
    if has_coordinates:
        longitude = float(longitude)
        latitude = float(latitude)
        # API request
        offset = request.GET.get('offset', default=0)
        limit = request.GET.get('limit', default=PAGINATION_LIMIT)
        url = ('https://api.resmio.com/v1/facility?near=%s,%s'
               '&verified=true&offset=%s&limit=%s&distance=100000' % 
                (longitude, latitude, offset, limit))
        response = requests.get(url).json()
        restaurants = response['objects']
        meta = response['meta']

        # base url for pagination
        base_url = 'nearby?longitude=%s&latitude=%s&' % \
                    (longitude, latitude)
        return render(request, 'nearby_list.html', 
            {'pagination_meta': meta,
             'current_position': [longitude, latitude],
             'pagination_base_url': base_url,
             'restaurants': restaurants})
    else:
        return render(request, 'geolocation.html')
