from django.utils.translation import ugettext_lazy as _

CATEGORIES = {
    'american': _('American'),
    'asianfusion': _('Asian fusion'),
    # 'austrian': _('Austrian'),
    'brewery': _('Brewery'),
    'cafebistro': _('Cafe & Bistro'),
    'chinese': _('Chinese'),
    'fish': _('Fish'),
    'german': _('German'),
    # 'french': _('French'),
    'gourmet': _('Gourmet'),
    'greek': _('Greek'),
    'indian': _('Indian'),
    'italian': _('Italian'),
    'lebanese': _('Libanese'),
    'mediterranean': _('Mediterranean'),
    'mexican': _('Mexican'),
    'spanish': _('Spanish'),
    'steakhouse': _('Steakhouse'),
    # 'sushi': _('Sushi'),
    # 'swiss': _('Swiss'),
    'thai': _('Thai'),
    'turkish': _('Turkish'),
}

def translate_category(category_id):
    return CATEGORIES[category_id]