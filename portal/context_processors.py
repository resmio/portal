from django.conf import settings

from portal.constants import URL_PARAMETERS_KEPT

def canonical_page(request):
	return {
	    'canonical_page': ''.join((
	        "http://", settings.SITE_DOMAIN, request.path
	    ))
	}

def parameters_kept(request):
    res = {}
    for param in URL_PARAMETERS_KEPT:
        res[param] = request.GET.get(param, False)
    return res

def site_domain(request):
    return {'SITE_DOMAIN': settings.SITE_DOMAIN}