from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView
from portal.sitemaps_view import sitemap_view

from portal.sitemap import sitemaps
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

# fix for patched url not working in gunicorn
# https://bitbucket.org/carljm/django-localeurl/issue/5/reverse-not-patched-on-apache-mod_wsgi
from localeurl.models import patch_reverse
patch_reverse()

urlpatterns = patterns('portal.views',
    # /
    url(r'^$', 'landing_page', name='landing_page'),

    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/img/favicon.ico')),
    url(r'^robots\.txt$', RedirectView.as_view(url='/static/robots.txt')),
    url(r'^sitemap\.xml$', sitemap_view, {'sitemaps': sitemaps}),
    # /home
    url(r'^home$', 'home', name='home'),
    # /impressum
    url(r'^impressum$', 'impressum', name='impressum'),
    # /nearby?longitude=53&latitude=10
    url(r'^nearby$', 
        'nearby', 
        name='nearby'),

    # /restaurant/restaurant-id
    url(r'^restaurant/(?P<restaurant_id>[-\w]+)$', 
        'restaurant', 
        name='restaurant'),
    # /city
    url(r'^(?P<city>[-\w]+)$', 
        'city_list', 
        name='city_list'),
    # /city/category
    url(r'^(?P<city>[-\w]+)/(?P<category>[-\w]+)$', 
        'category_list', 
        name='category_list'),
    # Examples
    # url(r'^portal/', include('portal.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
