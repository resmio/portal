import requests

featured = [
    'la-maison-de-lentrecote',
    'steackhouse-las-malvinas',
    'gasthaus-meyer',
    'restaurant-auberge-am-schlogut',
    'hindukusch',
    'petit-bonheur',
]

restaurants = []
for r_id in featured:
    url = 'https://api.resmio.com/v1/facility/%s' % r_id
    restaurants.append(requests.get(url).json())
print restaurants
