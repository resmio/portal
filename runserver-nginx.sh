#!/bin/bash
sudo service nginx start
[ -d /vagrant ] && gunicorn portal.wsgi --pythonpath=/vagrant || gunicorn portal.wsgi --pythonpath=/portal
