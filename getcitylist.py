#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import requests
import time
import sys

from portal.cityutils import restaurant_to_city_info

DEFAULT_RADIUS = 5

verbose = True

# get all restaurants from the API
if verbose:
    print 'calling API...'
restaurants = requests.get('https://api.resmio.com/v1/facility?verified=true'
                           '&limit=0&country=DE').json()['objects']

cities = {}
for r in restaurants:
    time.sleep(0.5)
    try:
        city_info = restaurant_to_city_info(r)
        city_id = city_info['id']
        if not cities.has_key(city_id):
            cities[city_id] = restaurant_to_city_info(r)
            cities[city_id]['n'] = 1
            cities[city_id]['categories'] = set(r['categories'] or [])
            if verbose:
                print "'" + city_id + "': " + repr(cities[city_id]) + ','
        else:
            # increment counter of number of restaurants in city
            cities[city_id]['n'] += 1
            if r['categories']:
                # add new categories
                cities[city_id]['categories'].update(set(r['categories']))
            if verbose:
                print '%s++' % city_id
    except Exception as e:
        sys.stderr.write(r['name'] + ': ' + str(e) + 
                         ' (location = ' + str(r['location']) + ')\n')

# set city locations from google geocoding info
for city_id in cities:
    time.sleep(0.5)
    city_info = cities[city_id]
    # the default radius will be overwritten if possible
    city_info['radius'] = DEFAULT_RADIUS
    url = ('http://maps.googleapis.com/maps/api/geocode/json?'
        'address=%s, Germany&sensor=true' % city_info['name'])
    response = requests.get(url).json()
    if response['status'] != 'OK':
        sys.stderr.write( 'google answered with %s! (city %s)\n' % 
                          (response['status'], city_info['id']) )
        continue

    if not response['results']:
        sys.stderr.write('no location found for "%s"!\n' % city_info['name'])
        continue
    first_result = response['results'][0]
    try:
        bounds = first_result['geometry']['bounds']
    except KeyError:
        sys.stderr.write('no bound data found for "%s"!\n' % city_info['name'])
        continue
    north = bounds['northeast']['lat']
    east  = bounds['northeast']['lng']
    south = bounds['southwest']['lat']
    west  = bounds['southwest']['lng']  
    # calculate center
    #lat = ( north + south ) / 2.0
    #lng = ( west + east ) / 2.0
    #lat = round(lat, 7)
    #lng = round(lng, 7)
    lat = first_result['geometry']['location']['lat']
    lng = first_result['geometry']['location']['lng']
    old_location = city_info['location']
    city_info['location'] = [lng, lat]
    height = abs(north-south) * 111.0
    width = abs(west-east) * 67.0
    # a very aproximate kilometric radius
    diameter = max(width, height) * 1.1
    city_info['radius'] = round( diameter/2.0, 2)
    if verbose:
        print 'location of "%s" to %s --> %s' % (city_info['id'], 
                                                 old_location,
                                                 city_info['location'])

if verbose:
    print '--------------------------------------------------------------------'
print '\nCITIES = {'
for c in cities.values():
    print '  ' + repr(c['id']) + ': {'
    for key, value in c.items():
        print '    ' + repr(key) + ': ' + repr(value) + ','
    print '  },'
print '}\n'


# city ids and the restaurant number in each
restaurant_numbers = [[x[0], x[1]['n']] for x in cities.items()]
restaurant_numbers = sorted(restaurant_numbers, key=lambda x: -x[1])

# city ids and the restaurant number in each
print 'SORTED_CITIES = [\n ',
print ',\n  '.join( [repr(x[0]) for x in restaurant_numbers] )
print ']' 


